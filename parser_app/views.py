import json
import time

import requests
from django.core import serializers
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView
# from pyresparser import ResumeParser
# from rest_framework.generics import get_object_or_404
from .forms import AssignResume, JobDescription_View, UploadJDModelForm
from .models import Resume, UploadResumeModelForm, JD_table
from django.contrib import messages
from django.conf import settings
from django.db import IntegrityError
from django.http import HttpResponse, FileResponse, Http404, JsonResponse
import os
from django.db.models import Q

from .configs import *

from rest_framework.filters import SearchFilter
from rest_framework.response import Response
from rest_framework import status, generics
from rest_framework.decorators import api_view

from django_filters.rest_framework import DjangoFilterBackend

from .serializers import JobDescriptionSerializer, ResumeSerializer
from .jd import Utils
from .resume import Utils2

import gensim
from gensim.models import FastText
from sklearn.metrics.pairwise import cosine_similarity

import itertools

print("load model")
model = FastText.load("./ml_models/fasttext_model/fasttext.model")
print("done load model")

es = connect_elasticsearch()
es_resumes = create_index(es, index_name='resumes')


def resumes_matching(searches):
    """
    Return a queryset for companies whose names contain case-insensitive
    matches for any of the `merchants`.
    """
    q = Q()
    for search in searches:
        q |= Q(name__icontains=search)
    return Resume.objects.filter(q)


def get_score_desig(text1, text2):
    try:
        scores = list()
        text1 = text1
        try:
            text2 = eval(text2)
        except:
            text2 = text2.split(",")
        for i in text1:
            max_score = 0
            for j in text2:
                s = cosine_similarity([model.wv[i.lower()]], [model[j.lower()]])[0][0]
                # print(f"i: {i.lower()},j:{j.lower()},{s}")
                if s > max_score and s > 0.90:
                    max_score = s

            if max_score != 0:
                scores.append(max_score)

            # print(scores)
        if len(scores) != 0:
            average = sum(scores) / len(scores)
        else:
            average = 0
    except:
        average = 0
    return average


def get_score(text1, text2):
    try:
        scores = list()
        text1 = text1
        try:
            text2 = eval(text2)
        except:
            text2 = text2.split(",")
        print(text1, text2)
        for i in text1:
            max_score = 0
            for j in text2:
                s = cosine_similarity([model.wv[i.lower()]], [model[j.lower()]])[0][0]
                # print(f"i: {i.lower()},j:{j.lower()},{s}")
                if s > max_score:
                    max_score = s
            scores.append(max_score)
        average = sum(scores) / len(scores)


    except:
        average = 0
    return average


def get_exp_weighted(jd_exp, resume_exp, max_weightage=0.30):
    score = 0
    jd_exp, resume_exp = float(jd_exp), float(resume_exp)
    print(jd_exp, resume_exp)
    if resume_exp == 0.0:
        score = 0
    elif resume_exp >= jd_exp:
        score = max_weightage
    else:
        score = max_weightage - (max_weightage * ((jd_exp - resume_exp) / jd_exp))
    return score


class APIError(Exception):
    """An API Error Exception"""

    def __init__(self, status):
        self.status = status

    def __str__(self):
        return "APIError: status={}".format(self.status)


@api_view(['POST'])
def job_description_upload(request):
    if request.method == 'POST':
        print("onside post")
        file_form = UploadJDModelForm(request.POST, request.FILES)
        print("FORM post")

        print("valid",file_form.is_valid())
        if file_form.is_valid():
            # saving the file
            print("inside files")
            if 'job_description' in request.FILES:

                file = request.FILES.get('job_description')

                job_description = JD_table(job_description=file)
                job_description.save()
                start = time.time()

                text = Utils.read_file(os.path.join(settings.MEDIA_ROOT, job_description.job_description.name))
                print(text)

                job_description.skills = Utils.skills(text)
                job_description.designation = Utils.job_designition(text)

                job_description.degree = Utils.get_degree2(text)
                job_description.experience = Utils.get_experience(text)

                job_description.save()
                print("TIMEEEEEE", time.time() - start)
                context = {
                    'file': job_description.job_description.url,
                    'degree': job_description.degree,
                    'designation': job_description.designation,
                    'skills': job_description.skills
                }

                return Response(context)
        else:
            print("else come")
            print("input records manually")
            job_description = JD_table()
            file_form.fields['summary'].required = True
            file_form.fields['skills'].required = True
            file_form.fields['designation'].required = True

            job_description.summary = request.POST.get('summary')
            job_description.designation = str(request.POST.get('designation')).replace(' ',",").split(',')
            job_description.skills = str(request.POST.get('skills')).replace(' ',",").split(',')
            job_description.save()



            return Response({
                'id': job_description.id,
                'designation': job_description.designation,
                'skills': job_description.skills})

    else:
        print("enter valid form")
        return Response({"fail":"failure"})
        # print("o said it")



# MATCHIN JD AND RESUMES IN THE DB
def job_description_fxn(request):
    form = UploadJDModelForm()
    Resume.objects.all().update(skills_score2=0, skills_score=0, designation_score=0, total_experience_score=0,
                                designation_score2=0, designation_score3=0, skills_score3=0)
    if request.method == 'POST':
        JD_table.objects.all().delete()
        file_form = UploadJDModelForm(request.POST, request.FILES)
        file = request.FILES.get('job_description')
        resumes_data = []
        if file_form.is_valid():
            # saving the file
            job_description = JD_table(job_description=file)
            job_description.save()

            text = Utils.read_file(os.path.join(settings.MEDIA_ROOT, job_description.job_description.name))
            print(text)

            job_description.skills = Utils.skills(text)
            job_description.designation = Utils.job_designition(text)

            job_description.degree = Utils.get_degree2(text)
            job_description.experience = Utils.get_experience(text)

            job_description.save()
            messages.success(request, 'Job Description uploaded!')
            print(job_description.skills)

            body = {
                "query": {
                    "bool": {
                        "should": [
                            {
                                "match": {
                                    "skills": ' '.join(list(job_description.skills))
                                }
                            },
                            {
                                "match": {
                                    "designation": ' '.join(list(job_description.designation))
                                }
                            }
                        ]
                    }
                }
            }
            body2 = {
                "query": {
                    "bool": {
                        "must": [
                            {
                                "match": {
                                    "skills": ' '.join(list(job_description.skills))
                                }
                            },
                            {
                                "match": {
                                    "designation": ' '.join(list(job_description.designation))
                                }
                            }
                        ]
                    }
                }
            }

            # search(es, es_resumes, body)
            searches = search(es, es_resumes, body2)

            names = [i['_source']['name'] for i in searches.get('hits').get('hits')]
            print("NAMEES--->AND", len(names), names)

            if names == []:
                searches = search(es, es_resumes, body)

                names = [i['_source']['name'] for i in searches.get('hits').get('hits')]
                print("NAMEES", names)

            # matching
            ess_resumes = resumes_matching(names)
            scores = list()
            for resume in ess_resumes:
                start = time.time()

                resume_skills = resume.skills
                resume_designation = resume.designation
                job_description_skills = job_description.skills
                job_description_designation = job_description.designation
                jd_exp = job_description.experience
                try:
                    resume_exp = float(resume.total_experience)
                except:
                    resume_exp = 0

                resume.skills_score = get_score(job_description_skills, resume_skills)

                # resume.skills_score2 = get_score2(job_description_skills, resume_skills)

                # resume.skills_score3 = get_score_desigone(job_description_designation, resume_designation)
                resume.designation_score3 = get_score_desig(job_description_designation, resume_designation)

                resume.save()

                # WEIGHTED MAX
                resume.designation_score2 = resume.skills_score * 0.40 + resume.designation_score3 * 0.30 + get_exp_weighted(
                    jd_exp, resume_exp, max_weightage=0.30)

                resume.save()
                print("TIMEEEEEE", time.time() - start)

            resumes = ess_resumes.order_by('-designation_score2')

            context = {
                'jd_skills': job_description.skills,
                'resumes': resumes,
                'job_description_designation': job_description_designation
            }
            return render(request, 'matching.html', context)

            # return redirect("/matching/")
    else:
        form = UploadJDModelForm()
    return render(request, 'description.html', {'form': form})


@api_view(['GET'])
def matching(request, job_description_id):
    # Resume.objects.all().update(skills_score2=0, skills_score=0, designation_score=0, total_experience_score=0,
    #                             designation_score2=0, designation_score3=0, skills_score3=0)

    job_description = get_object_or_404(JD_table, id=job_description_id)
    job_description_skills = eval(job_description.skills)
    job_description_designation = eval(job_description.designation)

    jd_exp = eval(job_description.experience)

    latest_resume_id = Resume.objects.last().id

    body = {
        "query": {
            "bool": {
                "should": [
                    {
                        "match": {
                            "skills": ' '.join(job_description_skills)
                        }
                    },
                    {
                        "match": {
                            "designation": ' '.join(job_description_designation)
                        }
                    }
                ]
            }
        }
    }

    body2 = {
        "query": {
            "bool": {
                "must": [
                    {
                        "match": {
                            "skills": ' '.join(job_description_skills)
                        }
                    },
                    {
                        "match": {
                            "designation": ' '.join(job_description_designation)
                        }
                    }
                ]
            }
        }
    }

    if job_description.matched_till == None:
        job_description.matched_till = 0

    if job_description.is_assigned == False:

        if job_description.matched_till == 0:

            print("matching from resume 0")

            start = time.time()

            # search(es, es_resumes, body)
            searches = search(es, es_resumes, body2)
            # print(searches)

            names = [i['_source']['name'] for i in searches.get('hits').get('hits')]
            print("NAMEES--->AND", len(names), names)

            if names == []:
                searches = search(es, es_resumes, body)

                names = [i['_source']['name'] for i in searches.get('hits').get('hits')]
                print("NAMEES", names)

                if names == []:
                    job_description.matched_till = Resume.objects.last().id
                    job_description.save()
                    return Response({'message': 'The  resumes dont suit the job description'})

            ess_resumes = resumes_matching(names)
            print("ess_resumes", ess_resumes)

            for resume in ess_resumes:

                resume_skills = resume.skills
                resume_designation = resume.designation

                try:
                    resume_exp = float(resume.total_experience)
                except:
                    resume_exp = 0

                resume.skills_score = get_score(job_description_skills, resume_skills)

                resume.designation_score3 = get_score_desig(job_description_designation, resume_designation)

                resume.save()

                # WEIGHTED MAX
                resume.designation_score2 = resume.skills_score * 0.40 + resume.designation_score3 * 0.30 + get_exp_weighted(
                    jd_exp, resume_exp, max_weightage=0.30)

                resume.save()

            resumes_result = list(
                ess_resumes.order_by('-designation_score2').values('id', 'resume', 'name', 'skills', 'designation',
                                                                   'education',
                                                                   'total_experience', 'designation_score2'))
            print("resumes_result", resumes_result)

            # Bind the top resumes to the Job Description
            top_resumes_results = ess_resumes.order_by('-designation_score2')
            job_description.top_resumes.add(*top_resumes_results)
            job_description.matched_till = Resume.objects.last().id
            job_description.save()

            print("TIMEEEEEE", time.time() - start)

            return JsonResponse(resumes_result, safe=False)

        elif latest_resume_id == job_description.matched_till:
            print(f"Resumes already matched till id {latest_resume_id}")
            context = {'message': f'Resumes already matched till {latest_resume_id}'}
            return Response(context)

        else:

            print(f"matching from resume {job_description.matched_till}")

            ess_resumes = Resume.objects.filter(id__gt=job_description.matched_till)
            # print("NEW RESUMES", ess_resumes)

            search_gt_latest = {
                "query": {
                    "bool": {
                        "must": [
                            {
                                "match": {
                                    "designation": ' '.join(job_description_designation)
                                }
                            },
                            {
                                "match": {
                                    "skills": ' '.join(job_description_skills)
                                }
                            }
                        ],
                        "filter": {
                            "range": {
                                "id": {
                                    "gt": job_description.matched_till,

                                }
                            }
                        }
                    }
                }
            }
            searches = search(es, es_resumes, search_gt_latest)
            # print(searches)

            names = [i['_source']['name'] for i in searches.get('hits').get('hits')]
            print("NAMEES--->3 queries", len(names), names)

            if names == []:
                job_description.matched_till = Resume.objects.last().id
                job_description.save()
                return Response({'message': 'The new resumes dont suit the job description'})

            ess_resumes = resumes_matching(names)

            for resume in ess_resumes:

                resume_skills = resume.skills
                resume_designation = resume.designation

                try:
                    resume_exp = float(resume.total_experience)
                except:
                    resume_exp = 0

                resume.skills_score = get_score(job_description_skills, resume_skills)

                resume.designation_score3 = get_score_desig(job_description_designation, resume_designation)

                resume.save()

                # WEIGHTED MAX
                resume.designation_score2 = resume.skills_score * 0.40 + resume.designation_score3 * 0.30 + get_exp_weighted(
                    jd_exp, resume_exp, max_weightage=0.30)

                resume.save()

            new_resumes_result = list(
                ess_resumes.order_by('-designation_score2').values('id', 'resume', 'name', 'skills', 'designation',
                                                                   'education',
                                                                   'total_experience', 'designation_score2'))

            # current top 20 resumes
            current_top20 = job_description.top_resumes.all()
            # new top 20
            new_top_resumes_results = ess_resumes.order_by('-designation_score2')

            # concatenate the 2 querysets and sort them by score
            new_tp_res = list(itertools.chain(current_top20, new_top_resumes_results))

            new_tp_res = [(i.id, i.designation_score2) for i in new_tp_res]
            print("new_top_resumes_results", new_tp_res)

            new_tp_res.sort(key=lambda tup: tup[1])
            new_tp_res = new_tp_res[::-1][:20]
            new_tp_res_ids = [i for (i, j) in new_tp_res]

            # clear first the resumes attached
            job_description.top_resumes.clear()

            # Bind the top resumes to the Job Description
            for i in new_tp_res_ids:
                job_description.top_resumes.add(Resume.objects.get(id=i))
            job_description.matched_till = Resume.objects.last().id
            job_description.save()
            # Elasticsearch().indices.delete("temp_new_resumes")
            # print("temp db deleted")

            return JsonResponse(new_resumes_result, safe=False)


    else:
        context = {
            'message': 'Sorry, job description is assigned to another resume'
        }
        return Response(context)


@api_view(['POST'])
def assign_job(request, job_description_id):
    job_description = get_object_or_404(JD_table, id=job_description_id)

    if job_description.is_assigned==True:
        context = {
            'message': f"The Job Description {job_description.id} is already assigned to another task"
        }
        return Response(context)

    assignments = list()

    if request.method == 'POST':
        form = AssignResume(request.POST)
        if form.is_valid():
            resumes = request.POST.getlist('resumes')

            if resumes:
                resumes = [int(x) for x in resumes]
                print(resumes)

                rs = Resume.objects.filter(id__in=resumes).order_by('-is_assigned')
                for r in rs:
                    if r.is_assigned == True:
                        context = {
                            'Message': f"Assignment not successful,Resume {r.id} is attached to another job"
                        }


                        return Response(context)

                    else:
                        query =Resume.objects.get(id=r.id)
                        query.is_assigned = True
                        query.save()

                        context = {
                            'Message': f"Assignment for Resume {r.id} is successful"
                        }

                        job_description.top_resumes.add(query)
                        job_description.save()

                        assignments.append(context)

                job_description.is_assigned = True

                job_description.save()
                print ("assignments>>>>>>>",assignments)
                return JsonResponse(assignments, safe=False)

        else:
            print("Pass resumes ids")



@api_view(['GET'])
def release_job(request, job_description_id):
    job_description = get_object_or_404(JD_table, id=job_description_id)

    job_description.is_assigned = False

    job_description.top_resumes.clear()



    job_description.save()

    context = {
        'Message': f"Job description {job_description_id} has been released"
    }

    return Response(context)

@api_view(['GET'])
def release_resume(request, id):
    resume = get_object_or_404(Resume, id=id)

    resume.is_assigned = False




    resume.save()

    context = {
        'Message': f"Resume {resume.id} has been released"
    }

    return Response(context)

@api_view(['GET'])
def open_job_descriptions(request):
    job_descriptions = JD_table.objects.filter(is_assigned=False)

    serializer = JobDescriptionSerializer(job_descriptions, many=True)
    return Response(serializer.data)


# Own Resume Parser
def resume_data2(request):
    form = UploadResumeModelForm()

    if request.method == 'POST':
        Resume.objects.all().delete()
        file_form = UploadResumeModelForm(request.POST, request.FILES)
        files = request.FILES.getlist('resume')
        resumes_data = []
        if file_form.is_valid():
            for file in files:
                try:
                    # saving the file

                    resume = Resume(resume=file)
                    resume.save()

                    # extracting resume entities
                    # parser = ResumeParser(os.path.join(settings.MEDIA_ROOT, resume.resume.name))
                    # data = parser.get_extracted_data()
                    # resumes_data.append(data)

                    data = Utils2.read_file(os.path.join(settings.MEDIA_ROOT, resume.resume.name))
                    print(data)

                    resume.name = data.get('name')
                    resume.email = data.get('email')
                    resume.mobile_number = data.get('phone')
                    resume.education = data.get('degree')
                    resume.skills = data.get('skills')
                    resume.college_name = data.get('university')
                    resume.designation = data.get('designition')
                    resume.total_experience = data.get('total_exp')

                    resume.save()
                    messages.success(request, 'Resumes uploaded!')

                except IntegrityError:
                    messages.warning(request, 'Duplicate resume found:', file.name)
                    return redirect('homepage')

            resumes = Resume.objects.all()

            context = {
                'resumes': resumes,
            }
            return render(request, 'resume.html', context)

    else:
        form = UploadResumeModelForm()

    context = {
        'form': form
    }

    return render(request, 'resume.html', context)


# 3rd Party Resume API
@api_view(['POST'])
def resume_data(request):
    form = UploadResumeModelForm()
    url = 'http://24.130.65.11:8000/api/v1/parser/resume'

    if request.method == 'POST':
        # Resume.objects.all().delete()

        file_form = UploadResumeModelForm(request.POST, request.FILES)
        files = request.FILES.getlist('resume')
        resumes_data = []
        if file_form.is_valid():
            for file in files:
                try:
                    # saving the file

                    resume = Resume(resume=file)
                    resume.save()

                    document = {'document': open(os.path.join(settings.MEDIA_ROOT, resume.resume.name), 'rb')}
                    # print("task>>>>>", document)

                    resp = requests.post(url, files=document)
                    # print("RESP____>>>>>", resp.text)

                    if resp.status_code != 200:
                        # raise APIError('POST /tasks/ {}'.format(resp.status_code))
                        continue

                    data = json.loads(resp.text)
                    print(data)

                    resume.name = data.get('name')
                    resume.email = data.get('email')
                    resume.mobile_number = data.get('mobile_number')
                    resume.education = data.get('degree')
                    resume.skills = data.get('skills')
                    resume.college_name = data.get('college_name')
                    resume.designation = data.get('designation')
                    resume.company_name = data.get('company_name')
                    resume.experience = data.get('experience')
                    resume.total_experience = data.get('total_experience')

                    resume.save()

                    record = {
                        "id": int(resume.id),
                        "name": resume.name,
                        "designation": resume.designation,
                        "skills": resume.skills,
                        "experience": resume.total_experience
                    }

                    store_record(elastic_object=es, index_name=es_resumes, record=record)

                    messages.success(request, 'Resumes uploaded!')

                except IntegrityError:
                    messages.warning(request, 'Duplicate resume found:', file.name)
                    return redirect('homepage')

            resumes = Resume.objects.all()

            context = {
                'resumes': resumes,
            }
            # return render(request, 'resume.html', context)
            return Response(data)

    else:
        form = UploadResumeModelForm()

    context = {
        'form': form
    }

    return render(request, 'resume.html', context)


class Job_Description_Search_View(generics.ListAPIView):
    queryset = JD_table.objects.all()
    serializer_class = JobDescriptionSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    # filterset_fields = ['skills', ]
    search_fields = ['$skills','$designation','$summary']


class Resume_Search_View(generics.ListAPIView):
    queryset = Resume.objects.all()
    serializer_class = ResumeSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    search_fields = ['$skills','$designation']




class Resume_Search_open(generics.ListAPIView):
    queryset = Resume.objects.filter(is_assigned = False)
    serializer_class = ResumeSerializer



@api_view(['POST'])
def view_resumes(request):

    if request.method == 'POST':
        form = AssignResume(request.POST)
        if form.is_valid():
            resumes = request.POST.getlist('resumes')

            if resumes:
                resumes = [int(x) for x in resumes]
                print(resumes)

                rs = Resume.objects.filter(id__in=resumes)
                serializer = ResumeSerializer(rs,many=True)

                return Response (serializer.data)

        else:
            print("Pass resumes ids")

@api_view(['POST'])
def view_job_description(request):

    if request.method == 'POST':
        form = JobDescription_View(request.POST)
        if form.is_valid():
            job_descriptions = request.POST.getlist('job_descriptions')

            if job_descriptions:
                job_descriptions = [int(x) for x in job_descriptions]
                print(job_descriptions)

                rs = JD_table.objects.filter(id__in=job_descriptions)
                serializer = JobDescriptionSerializer(rs,many=True)

                return Response(serializer.data)

        else:
            print("Pass JobDescriptions ids")
