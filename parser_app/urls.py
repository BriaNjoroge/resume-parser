"""resume_parser.parser_app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.urls import path, include
from django.views.generic import TemplateView
from rest_framework.urlpatterns import format_suffix_patterns

from .models import JD_table
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
                  # path('', views.job_description, name='job_description'),
                  # upload job description and resumes
                  path('api/v1/parser/resume/upload', views.resume_data, name='resume_data'),
                  path('api/v1/parser/jobdescription/upload', views.job_description_upload, name='job_description_upload'),

                  # match job description and resumes
                  path('api/v1/parser/jobdescription/<int:job_description_id>/match', views.matching, name='matching'),

                  # View open resumes and job descriptions
                  path('api/v1/parser/jobdescription/open', views.open_job_descriptions, name='open_job_descriptions'),
                  path('api/v1/parser/resume/open', views.Resume_Search_open.as_view(), name='open_resume'),

                  # Assign and Release Resume to job desicription
                  path('api/v1/parser/jobdescription/<int:job_description_id>/assign',
                       views.assign_job, name='assign_job'),
                  path('api/v1/parser/jobdescription/<int:job_description_id>/release',
                       views.release_job, name='release_job'),

                  path('api/v1/parser/resume/<int:id>/release',
                       views.release_resume, name='release_resume'),

                  # search resume and job description by skill and designation
                  path('api/v1/search/jobdescription', views.Job_Description_Search_View.as_view(),
                       name='view_job_descriptions'),
                  path('api/v1/search/resume', views.Resume_Search_View.as_view(),
                       name='Resume_Search_View'),

                  # View Job descriptions an resumes by id
                  path('api/v1/parser/jobdescription/view', views.view_job_description,
                       name='view_job_description'),
                  path('api/v1/parser/resume/view', views.view_resumes,
                       name='resume_view'),



              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns = format_suffix_patterns(urlpatterns)
