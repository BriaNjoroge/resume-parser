from django.db import models
from django import forms
from django.forms import ClearableFileInput

# for deleting media files after record is deleted
from django.db.models.signals import post_delete
from django.dispatch import receiver


class Resume(models.Model):
    resume = models.FileField('Upload Resumes', upload_to='resumes/')
    name = models.CharField('Name', max_length=255, null=True, blank=True)
    email = models.CharField('Email', max_length=255, null=True, blank=True)
    mobile_number = models.CharField('Mobile Number', max_length=255, null=True, blank=True)
    education = models.CharField('Education', max_length=255, null=True, blank=True)
    skills = models.CharField('Skills', max_length=1000, null=True, blank=True)
    company_name = models.CharField('Company Name', max_length=1000, null=True, blank=True)
    college_name = models.CharField('College Name', max_length=1000, null=True, blank=True)
    designation = models.CharField('Designation', max_length=1000, null=True, blank=True)
    experience = models.CharField('Experience', max_length=1000, null=True, blank=True)
    uploaded_on = models.DateTimeField('Uploaded On', auto_now_add=True)
    total_experience = models.CharField('Total Experience (in Years)', max_length=1000, null=True, blank=True)
    skills_score = models.FloatField('Skills Score', default=0)
    designation_score3 = models.FloatField('Designation Score', default=0)
    designation_score2 = models.FloatField('Total Score', default=0)

    is_assigned = models.BooleanField('Assigned_Resume', default=False)


class UploadResumeModelForm(forms.ModelForm):
    class Meta:
        model = Resume
        fields = ['resume']
        widgets = {
            'resume': ClearableFileInput(attrs={'multiple': True}),
        }


# delete the resume files associated with each object or record
@receiver(post_delete, sender=Resume)
def submission_delete(sender, instance, **kwargs):
    instance.resume.delete(False)


class JD_table(models.Model):
    job_description = models.FileField('Upload Descriptions', upload_to='job_description/')
    summary = models.CharField('Summary', max_length=255, null=True, blank=True)

    skills = models.CharField('skills', max_length=255, null=True, blank=True)
    experience = models.CharField('Experience', max_length=255, null=True, blank=True)
    degree = models.CharField('Degree', max_length=255, null=True, blank=True)
    designation = models.CharField('Designation', max_length=255, null=True, blank=True)

    is_assigned = models.BooleanField('Assigned_Task', default=False)

    matched_till = models.IntegerField(blank=True, null=True)
    top_resumes = models.ManyToManyField(Resume, blank=True)




# delete the resume files associated with each object or record
@receiver(post_delete, sender=JD_table)
def submission_delete(sender, instance, **kwargs):
    instance.job_description.delete(False)