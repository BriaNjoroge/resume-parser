import gensim
from gensim.models import FastText
from sklearn.metrics.pairwise import cosine_similarity

model = FastText.load("./ml_models/fasttext_model/fasttext.model")


class match:
    # print("AAAAA")
    def get_score(text1, text2):
        return cosine_similarity([model.wv[text1]], [model[text2]])[0][0]

# print(get_score('pytorch keras sklearn tensorflow','finance business power bi'))
# a = ['pytorch', 'keras', 'sklearn', 'tensorflow']
# b = ["finance", "business","power bi"]
# compare = []
# for i in a:
#   for j in b:
#     compare.append(get_score(i,j))
# sum(compare)/ len(compare)
