from django import forms
from django.forms import ClearableFileInput

from parser_app.models import Resume, JD_table


class AssignResume(forms.Form):
    resumes = forms.ModelChoiceField(queryset=Resume.objects.all(),label='Select Resumes',  widget=forms.Select(attrs={'class': "form-control"}))


class JobDescription_View(forms.Form):
    job_descriptions = forms.ModelChoiceField(queryset=JD_table.objects.all(),label='Select Job Descriptions',  widget=forms.Select(attrs={'class': "form-control"}))

class UploadJDModelForm(forms.ModelForm):
    class Meta:
        model = JD_table
        fields = ['job_description','summary','skills','designation','degree']
        widgets = {
            'job_description': ClearableFileInput(attrs={'job_description': True}),
        }

# class UploadJDModelForm(forms.ModelForm):
#     class Meta:
#         model = JD_table
#         fields = ['job_description','summary','skills','designation','degree']
#         widgets = {
#             'job_description': ClearableFileInput(attrs={'job_description': True}),
#         }
class UploadJDModelForm(forms.ModelForm):
    class Meta:
        model = JD_table
        fields = ['job_description','summary','skills','designation','degree']
        widgets = {
            'job_description': ClearableFileInput(attrs={'job_description': True}),
        }

