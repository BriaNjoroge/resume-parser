from elasticsearch import Elasticsearch

#@title Connect the ElasticSearch server
def connect_elasticsearch():
    _es = None
    _es = Elasticsearch([{'host': 'localhost', 'port': 9200}])
    if _es.ping():
        print('Elasticsearch successfully connected')
    else:
        print('Elasticsearch could not connect!')
    return _es


#@title Creating Index
def create_index(es_object, index_name='resumes'):
    created = False
    # index settings
    settings = {
        "settings": {
            "number_of_shards": 1,
            "number_of_replicas": 0
        },
        "mappings": {
                "dynamic": "strict",
                "properties": {
                    "id": {
                        "type": "integer"
                    },
                    "name": {
                        "type": "text"
                    },
                    "designation": {
                        "type": "nested",
                        "properties": {
                            "step": {"type": "text"}
                        }
                    },
                    "skills": {
                        "type": "nested",
                        "properties": {
                            "step": {"type": "text"}
                        }
                    },
                 "experience": {
                        "type": "integer"
                    }
        }
    }
    }
    try:
        if not es_object.indices.exists(index_name):
            # Ignore 400 means to ignore "Index Already Exist" error.
            es_object.indices.create(index=index_name, ignore=400, body=settings)
            print(f'Created Index--> {index_name}')
        created = True
    except Exception as ex:
        print(str(ex))
    finally:
        return created

#@title Recording Indexing
def store_record(elastic_object, index_name, record):
    try:
        outcome = elastic_object.index(index=index_name, body=record)
        print(f'{outcome} stored')
        return f'{outcome} stored'
    except Exception as ex:
        print('Error in indexing data')
        print(str(ex))

#@title Querying Records
def search(es_object, index_name, search):
    res = es_object.search(index=index_name, body=search,size=20)
    return res