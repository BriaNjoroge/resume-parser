from rest_framework import serializers
from .models import *

class JobDescriptionSerializer(serializers.ModelSerializer):

    class Meta:
        model = JD_table
        fields = ['id','job_description']

class ResumeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Resume
        fields = ['id','resume','name']
