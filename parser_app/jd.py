# !apt-get install python-dev libxml2-dev libxslt1-dev antiword unrtf poppler-utils pstotext tesseract-ocr 
# !sudo apt-get install libenchant1c2a

# !pip install tika
# !pip install docx2txt
# !pip install phonenumbers
# !pip install pyenchant
# !pip install stemming
# !pip install word2number
from __future__ import division

import nltk
# nltk.download('punkt')
# nltk.download('averaged_perceptron_tagger')
# nltk.download('universal_tagset')
# nltk.download('maxent_ne_chunker')
# nltk.download('stopwords')
# nltk.download('wordnet')
# nltk.download('brown')

import sys
import operator
import string
import nltk
from stemming.porter2 import stem

import re
import pdfplumber

import nltk
import docx2txt
from tika import parser
import phonenumbers
import pandas as pd

from word2number import w2n

import os

import spacy
from spacy.matcher import Matcher
from spacy.matcher import PhraseMatcher

import logging

# load pre-trained model
nlp = spacy.load('en_core_web_sm')

custom_nlp2 = spacy.load("./ml_models/degree/content/model/")

# initialize matcher with a vocab
matcher = Matcher(nlp.vocab)

file = os.path.join("./util_files", 'titles_combined.txt')
file = open(file, "r", encoding='latin')
designation = file.readlines()
designation = [re.sub('\n', '', i).lower() for i in designation]
designitionmatcher = PhraseMatcher(nlp.vocab)
terms = designation
patterns = [nlp.make_doc(text) for text in terms if len(nlp.make_doc(text)) < 10]
designitionmatcher.add("Job title", None, *patterns)

file = os.path.join("./util_files", 'LINKEDIN_SKILLS_ORIGINAL.txt')
file = open(file, "r", encoding='latin')
skill = file.readlines()
skill = [re.sub('\n', '', i).lower() for i in skill]
skillsmatcher = PhraseMatcher(nlp.vocab)
terms = skill
patterns = [nlp.make_doc(text) for text in terms if len(nlp.make_doc(text)) < 10]
skillsmatcher.add("Job title", None, *patterns)


class Utils:

    def convert_docx_to_txt(docx_file):
        """
            A utility function to convert a Microsoft docx files to raw text.

            This code is largely borrowed from existing solutions, and does not match the style of the rest of this repo.
            :param docx_file: docx file with gets uploaded by the user
            :type docx_file: InMemoryUploadedFile
            :return: The text contents of the docx file
            :rtype: str
        """
        # try:
        #     text = docx2txt.process(docx_file)  # Extract text from docx file
        #     clean_text = text.replace("\r", "\n").replace("\t", " ")  # Normalize text blob
        #     resume_lines = clean_text.splitlines()  # Split text blob into individual lines
        #     resume_lines = [re.sub('\s+', ' ', line.strip()) for line in resume_lines if line.strip()]  # Remove empty strings and whitespaces

        #     return resume_lines
        # except KeyError:
        #     text = textract.process(docx_file)
        #     text = text.decode("utf-8")
        #     clean_text = text.replace("\r", "\n").replace("\t", " ")  # Normalize text blob
        #     resume_lines = clean_text.splitlines()  # Split text blob into individual lines
        #     resume_lines = [re.sub('\s+', ' ', line.strip()) for line in resume_lines if line.strip()]  # Remove empty strings and whitespaces
        #     return resume_lines
        try:

            text = parser.from_file(docx_file, service='text')['content']
            clean_text = re.sub(r'\n+', '\n', text)
            clean_text = clean_text.replace("\r", "\n").replace("\t", " ")  # Normalize text blob
            resume_lines = clean_text.splitlines()  # Split text blob into individual lines
            resume_lines = [re.sub('\s+', ' ', line.strip()) for line in resume_lines if
                            line.strip()]  # Remove empty strings and whitespaces
            return resume_lines, text
        except Exception as e:
            logging.error('Error in docx file:: ' + str(e))
            return [], " "

    def convert_pdf_to_txt(pdf_file):
        """
        A utility function to convert a machine-readable PDF to raw text.

        This code is largely borrowed from existing solutions, and does not match the style of the rest of this repo.
        :param input_pdf_path: Path to the .pdf file which should be converted
        :type input_pdf_path: str
        :return: The text contents of the pdf
        :rtype: str
        """
        try:
            # PDFMiner boilerplate
            # pdf = pdfplumber.open(pdf_file)
            # full_string= ""
            # for page in pdf.pages:
            #   full_string += page.extract_text() + "\n"
            # pdf.close()

            raw_text = parser.from_file(pdf_file, service='text')['content']

            full_string = re.sub(r'\n+', '\n', raw_text)
            full_string = full_string.replace("\r", "\n")
            full_string = full_string.replace("\t", " ")

            # Remove awkward LaTeX bullet characters

            full_string = re.sub(r"\uf0b7", " ", full_string)
            full_string = re.sub(r"\(cid:\d{0,2}\)", " ", full_string)
            full_string = re.sub(r'• ', " ", full_string)

            # Split text blob into individual lines
            resume_lines = full_string.splitlines(True)

            # Remove empty strings and whitespaces
            resume_lines = [re.sub('\s+', ' ', line.strip()) for line in resume_lines if line.strip()]
            print("PDF___________", resume_lines)
            return resume_lines, raw_text

        except Exception as e:
            logging.error('Error in pdf file:: ' + str(e))
            return [], " "

    def find_phone(text):
        try:
            return list(iter(phonenumbers.PhoneNumberMatcher(text, None)))[0].raw_string
        except:
            try:
                return re.search(
                    r'(\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4})',
                    text).group()
            except:
                return ""

    def extract_email(text):
        email = re.findall(r"([^@|\s]+@[^@]+\.[^@|\s]+)", text)
        if email:
            try:
                return email[0].split()[0].strip(';')
            except IndexError:
                return None

    def extract_name(resume_text):
        nlp_text = nlp(resume_text)

        # First name and Last name are always Proper Nouns
        # pattern_FML = [{'POS': 'PROPN', 'ENT_TYPE': 'PERSON', 'OP': '+'}]

        pattern = [{'POS': 'PROPN'}, {'POS': 'PROPN'}]
        matcher.add('NAME', None, pattern)

        matches = matcher(nlp_text)

        for match_id, start, end in matches:
            span = nlp_text[start:end]
            return span.text
        return ""

    def skills(text):

        skills = []

        __nlp = nlp(text.lower())
        # Only run nlp.make_doc to speed things up

        matches = skillsmatcher(__nlp)
        for match_id, start, end in matches:
            span = __nlp[start:end]
            skills.append(span.text)
        skills = list(set(skills))
        return skills

    def job_designition(text):

        job_titles = []

        __nlp = nlp(text.lower())
        # Only run nlp.make_doc to speed things up

        matches = designitionmatcher(__nlp)
        for match_id, start, end in matches:
            span = __nlp[start:end]
            job_titles.append(span.text)
        job_titles = list(set(job_titles))
        return job_titles

    def get_degree2(text):
        doc = custom_nlp2(text)
        degree = []

        degree = [ent.text.replace("\n", " ") for ent in list(doc.ents) if ent.label_ == 'Degree']
        return list(dict.fromkeys(degree).keys())

    def is_punctuation(word):
        return len(word) == 1 and word in string.punctuation

    def is_numeric(word):
        try:
            float(word) if '.' in word else int(word)
            return True
        except ValueError:
            return False

    # class KeywordExtractor:
    #     """Extracts keywords and keyphrases from text input"""
    #
    #     def __init__(self):
    #         self.stopwords = set(nltk.corpus.stopwords.words())
    #         self.top_fraction = 1
    #
    #     def _generate_candidate_keywords(self, sentences, max_length=3):
    #         """Creates a list of candidate keywords, or phrases of at most max_length words, from a set of sentences"""
    #         phrase_list = []
    #         for sentence in sentences:
    #             words = map(lambda x: "|" if x in self.stopwords else x,
    #                         nltk.word_tokenize(sentence.lower()))
    #
    #             phrase = []
    #             for word in words:
    #                 if word == "|" or Utils.is_punctuation(word):
    #                     if len(phrase) > 0:
    #                         if len(phrase) <= max_length:
    #                             phrase_list.append(phrase)
    #                         phrase = []
    #                 else:
    #                     phrase.append(word)
    #             if len(phrase):
    #                 phrase_list.append(phrase)
    #
    #         return phrase_list
    #
    #     def _calculate_word_scores(self, phrase_list):
    #         """Scores words according to frequency and tendency to appear in multi-word key phrases"""
    #         word_freq = nltk.FreqDist()
    #         word_multiplier = nltk.FreqDist()
    #         for phrase in phrase_list:
    #             # Give a higher score if word appears in multi-word candidates
    #             multi_word = min(2, len(list(filter(lambda x: not Utils.is_numeric(x), phrase))))
    #             for word in phrase:
    #                 # Normalize by taking the stem
    #                 word_freq[stem(word)] += 1
    #                 word_multiplier[stem(word)] += multi_word
    #         for word in word_freq.keys():
    #             word_multiplier[word] = word_multiplier[word] / float(word_freq[word])  # Take average
    #         word_scores = {}
    #         for word in word_freq.keys():
    #             word_scores[word] = word_freq[word] * word_multiplier[word]
    #
    #         return word_scores
    #
    #     def _calculate_phrase_scores(self, phrase_list, word_scores, metric='avg'):
    #         """Scores phrases by taking the average, sum, or max of the scores of its words"""
    #         phrase_scores = {}
    #         for phrase in phrase_list:
    #             phrase_score = 0
    #             if metric in ['avg', 'sum']:
    #                 for word in phrase:
    #                     phrase_score += word_scores[stem(word)]
    #                 phrase_scores[" ".join(phrase)] = phrase_score
    #                 if metric == 'avg':
    #                     phrase_scores[" ".join(phrase)] = phrase_score / float(len(phrase))
    #             elif metric == 'max':
    #                 for word in phrase:
    #                     phrase_score = word_scores[stem(word)] if word_scores[
    #                                                                   stem(word)] > phrase_score else phrase_score
    #                 phrase_scores[" ".join(phrase)] = phrase_score
    #
    #         return phrase_scores
    #
    #     def extract(self, text, max_length=3, metric='avg', incl_scores=True):
    #         """Extract keywords and keyphrases from input text in descending order of score"""
    #         sentences = nltk.tokenize.line_tokenize(text)
    #         # sentences = nltk.sent_tokenize(text)
    #         phrase_list = self._generate_candidate_keywords(sentences, max_length=max_length)
    #
    #         word_scores = self._calculate_word_scores(phrase_list)
    #         phrase_scores = self._calculate_phrase_scores(phrase_list, word_scores, metric=metric)
    #         sorted_phrase_scores = sorted(phrase_scores.items(), key=operator.itemgetter(1), reverse=True)
    #         n_phrases = len(sorted_phrase_scores)
    #
    #         if incl_scores:
    #             return sorted_phrase_scores[0:int(n_phrases / self.top_fraction)]
    #         else:
    #             return map(lambda x: x[0], sorted_phrase_scores[0:int(n_phrases / self.top_fraction)])

    # def extract_skills(text_string, file):
    #     all_skills = list()
    #
    #     with open(file, 'r', encoding='utf-8') as input_file:
    #         for line in input_file:
    #             all_skills.append(line.strip().lower())
    #     extractor = Utils.KeywordExtractor()
    #
    #     skills = [s for s in extractor.extract(str(text_string).lower()) if s in all_skills]
    #
    #     return skills

    def get_experience(text):
        experience = 0
        for m in re.finditer(r"((year[\D]+?(\w*\s)*?)(experience)|(year(\w*\s)*?)(work([a-zA-Z]*)))", text):
            print(m.start(0) - 15 if m.start(0) - 15 >= 0 else 0, m.end(0),
                  text[m.start(0) - 15 if m.start(0) - 15 >= 0 else 0: m.end(0)], end="")
            candidate = text[m.start(0) - 15 if m.start(0) - 15 >= 0 else 0: m.end(0)]
            min_exp = re.findall("\d+", candidate)
            if min_exp == []:
                new_msg = ""
                token = candidate.split()
                for i in token:
                    try:
                        new_msg += str(w2n.word_to_num(i)) + " "
                    except:
                        new_msg += str(i) + " "
                min_exp = re.findall("\d+", new_msg)
                try:
                    min_exp = min(list(map(int, min_exp)))
                    if experience > min_exp:
                        experience = min_exp
                except:
                    pass

        return experience

    def read_file(file):

        if file.endswith('docx') or file.endswith('doc'):
            resume_lines, raw_text = Utils.convert_docx_to_txt(file)
        elif file.endswith('pdf'):
            # resume_lines, raw_text = Utils.convert_pdf_to_txt(file)

            pdf = pdfplumber.open(file)
            full_text= ""
            for page in pdf.pages:
              full_text += page.extract_text() + "\n"
            pdf.close()
            resume_lines = full_text.split(' ')

        elif file.endswith('txt'):

            with open(file, 'r', encoding='latin') as f:
                resume_lines = f.readlines()


        else:
            resume_lines = None

        full_text = " ".join(resume_lines)

        return full_text

# if __name__ == "__main__":
#     file = "jobdescription.pdf"
#     file = os.path.join(file)
#
#     if file.endswith('docx') or file.endswith('doc') :
#         resume_lines, raw_text = convert_docx_to_txt(file)
#     elif file.endswith('pdf'):
#         resume_lines, raw_text = convert_pdf_to_txt(file)
#     else:
#         resume_lines = None
#
#     full_text = " ".join(resume_lines)
#
#     phone = find_phone(full_text)
#     email = extract_email(full_text)
#
#     degree = get_degree2(full_text)
#
#     skills = " ".join(extract_skills(raw_text,"LINKEDIN_SKILLS_ORIGINAL.txt"))
#
#     exp = get_experience(full_text)
#     if exp != []:
#         import itertools
#         min_exp = min(list(map(int, itertools.chain(*exp))))
#     else:
#         min_exp = 0
#     min_exp
#
#
#     designition = job_designition(full_text,"titles_combined.txt")
#     designition = list(dict.fromkeys(designition).keys())
